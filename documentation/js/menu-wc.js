'use strict';

customElements.define('compodoc-menu', class extends HTMLElement {
    constructor() {
        super();
        this.isNormalMode = this.getAttribute('mode') === 'normal';
    }

    connectedCallback() {
        this.render(this.isNormalMode);
    }

    render(isNormalMode) {
        let tp = lithtml.html(`
        <nav>
            <ul class="list">
                <li class="title">
                    <a href="index.html" data-type="index-link">leave-management documentation</a>
                </li>

                <li class="divider"></li>
                ${ isNormalMode ? `<div id="book-search-input" role="search"><input type="text" placeholder="Type to search"></div>` : '' }
                <li class="chapter">
                    <a data-type="chapter-link" href="index.html"><span class="icon ion-ios-home"></span>Getting started</a>
                    <ul class="links">
                        <li class="link">
                            <a href="overview.html" data-type="chapter-link">
                                <span class="icon ion-ios-keypad"></span>Overview
                            </a>
                        </li>
                        <li class="link">
                            <a href="index.html" data-type="chapter-link">
                                <span class="icon ion-ios-paper"></span>README
                            </a>
                        </li>
                                <li class="link">
                                    <a href="dependencies.html" data-type="chapter-link">
                                        <span class="icon ion-ios-list"></span>Dependencies
                                    </a>
                                </li>
                    </ul>
                </li>
                    <li class="chapter modules">
                        <a data-type="chapter-link" href="modules.html">
                            <div class="menu-toggler linked" data-toggle="collapse" ${ isNormalMode ?
                                'data-target="#modules-links"' : 'data-target="#xs-modules-links"' }>
                                <span class="icon ion-ios-archive"></span>
                                <span class="link-name">Modules</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                        </a>
                        <ul class="links collapse " ${ isNormalMode ? 'id="modules-links"' : 'id="xs-modules-links"' }>
                            <li class="link">
                                <a href="modules/AppModule.html" data-type="entity-link" >AppModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#controllers-links-module-AppModule-2be970c147f2d463bc6efab0f12f185a4ec7c12b20333110b3a406912e0418a86eedb51fb35bfaaadf9fef5407f12acc298c81867bacb2de783afd3bd03c8a97"' : 'data-target="#xs-controllers-links-module-AppModule-2be970c147f2d463bc6efab0f12f185a4ec7c12b20333110b3a406912e0418a86eedb51fb35bfaaadf9fef5407f12acc298c81867bacb2de783afd3bd03c8a97"' }>
                                            <span class="icon ion-md-swap"></span>
                                            <span>Controllers</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="controllers-links-module-AppModule-2be970c147f2d463bc6efab0f12f185a4ec7c12b20333110b3a406912e0418a86eedb51fb35bfaaadf9fef5407f12acc298c81867bacb2de783afd3bd03c8a97"' :
                                            'id="xs-controllers-links-module-AppModule-2be970c147f2d463bc6efab0f12f185a4ec7c12b20333110b3a406912e0418a86eedb51fb35bfaaadf9fef5407f12acc298c81867bacb2de783afd3bd03c8a97"' }>
                                            <li class="link">
                                                <a href="controllers/AppController.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >AppController</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-AppModule-2be970c147f2d463bc6efab0f12f185a4ec7c12b20333110b3a406912e0418a86eedb51fb35bfaaadf9fef5407f12acc298c81867bacb2de783afd3bd03c8a97"' : 'data-target="#xs-injectables-links-module-AppModule-2be970c147f2d463bc6efab0f12f185a4ec7c12b20333110b3a406912e0418a86eedb51fb35bfaaadf9fef5407f12acc298c81867bacb2de783afd3bd03c8a97"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-AppModule-2be970c147f2d463bc6efab0f12f185a4ec7c12b20333110b3a406912e0418a86eedb51fb35bfaaadf9fef5407f12acc298c81867bacb2de783afd3bd03c8a97"' :
                                        'id="xs-injectables-links-module-AppModule-2be970c147f2d463bc6efab0f12f185a4ec7c12b20333110b3a406912e0418a86eedb51fb35bfaaadf9fef5407f12acc298c81867bacb2de783afd3bd03c8a97"' }>
                                        <li class="link">
                                            <a href="injectables/AppService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >AppService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/LeaveModule.html" data-type="entity-link" >LeaveModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#controllers-links-module-LeaveModule-97409645768ff7b6ee4f5ccd07668581cf53d36eb793131b73b379c4c13ba286b2946dc48a277dc54e73baac73ddbd4521ed342e3e12192bd8655e6b5fa0df0f"' : 'data-target="#xs-controllers-links-module-LeaveModule-97409645768ff7b6ee4f5ccd07668581cf53d36eb793131b73b379c4c13ba286b2946dc48a277dc54e73baac73ddbd4521ed342e3e12192bd8655e6b5fa0df0f"' }>
                                            <span class="icon ion-md-swap"></span>
                                            <span>Controllers</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="controllers-links-module-LeaveModule-97409645768ff7b6ee4f5ccd07668581cf53d36eb793131b73b379c4c13ba286b2946dc48a277dc54e73baac73ddbd4521ed342e3e12192bd8655e6b5fa0df0f"' :
                                            'id="xs-controllers-links-module-LeaveModule-97409645768ff7b6ee4f5ccd07668581cf53d36eb793131b73b379c4c13ba286b2946dc48a277dc54e73baac73ddbd4521ed342e3e12192bd8655e6b5fa0df0f"' }>
                                            <li class="link">
                                                <a href="controllers/LeaveController.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >LeaveController</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-LeaveModule-97409645768ff7b6ee4f5ccd07668581cf53d36eb793131b73b379c4c13ba286b2946dc48a277dc54e73baac73ddbd4521ed342e3e12192bd8655e6b5fa0df0f"' : 'data-target="#xs-injectables-links-module-LeaveModule-97409645768ff7b6ee4f5ccd07668581cf53d36eb793131b73b379c4c13ba286b2946dc48a277dc54e73baac73ddbd4521ed342e3e12192bd8655e6b5fa0df0f"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-LeaveModule-97409645768ff7b6ee4f5ccd07668581cf53d36eb793131b73b379c4c13ba286b2946dc48a277dc54e73baac73ddbd4521ed342e3e12192bd8655e6b5fa0df0f"' :
                                        'id="xs-injectables-links-module-LeaveModule-97409645768ff7b6ee4f5ccd07668581cf53d36eb793131b73b379c4c13ba286b2946dc48a277dc54e73baac73ddbd4521ed342e3e12192bd8655e6b5fa0df0f"' }>
                                        <li class="link">
                                            <a href="injectables/LeaveService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >LeaveService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                </ul>
                </li>
                        <li class="chapter">
                            <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#controllers-links"' :
                                'data-target="#xs-controllers-links"' }>
                                <span class="icon ion-md-swap"></span>
                                <span>Controllers</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                            <ul class="links collapse " ${ isNormalMode ? 'id="controllers-links"' : 'id="xs-controllers-links"' }>
                                <li class="link">
                                    <a href="controllers/AppController.html" data-type="entity-link" >AppController</a>
                                </li>
                                <li class="link">
                                    <a href="controllers/LeaveController.html" data-type="entity-link" >LeaveController</a>
                                </li>
                            </ul>
                        </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#classes-links"' :
                            'data-target="#xs-classes-links"' }>
                            <span class="icon ion-ios-paper"></span>
                            <span>Classes</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="classes-links"' : 'id="xs-classes-links"' }>
                            <li class="link">
                                <a href="classes/HttpExceptionFilter.html" data-type="entity-link" >HttpExceptionFilter</a>
                            </li>
                            <li class="link">
                                <a href="classes/User.html" data-type="entity-link" >User</a>
                            </li>
                            <li class="link">
                                <a href="classes/UserLeave.html" data-type="entity-link" >UserLeave</a>
                            </li>
                            <li class="link">
                                <a href="classes/ValidationExceptionFilter.html" data-type="entity-link" >ValidationExceptionFilter</a>
                            </li>
                        </ul>
                    </li>
                        <li class="chapter">
                            <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#injectables-links"' :
                                'data-target="#xs-injectables-links"' }>
                                <span class="icon ion-md-arrow-round-down"></span>
                                <span>Injectables</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                            <ul class="links collapse " ${ isNormalMode ? 'id="injectables-links"' : 'id="xs-injectables-links"' }>
                                <li class="link">
                                    <a href="injectables/AppService.html" data-type="entity-link" >AppService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/LeaveService.html" data-type="entity-link" >LeaveService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/LoggerMiddleware.html" data-type="entity-link" >LoggerMiddleware</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/ValidationPipes.html" data-type="entity-link" >ValidationPipes</a>
                                </li>
                            </ul>
                        </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#interfaces-links"' :
                            'data-target="#xs-interfaces-links"' }>
                            <span class="icon ion-md-information-circle-outline"></span>
                            <span>Interfaces</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? ' id="interfaces-links"' : 'id="xs-interfaces-links"' }>
                            <li class="link">
                                <a href="interfaces/SwaggerConfig.html" data-type="entity-link" >SwaggerConfig</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#miscellaneous-links"'
                            : 'data-target="#xs-miscellaneous-links"' }>
                            <span class="icon ion-ios-cube"></span>
                            <span>Miscellaneous</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="miscellaneous-links"' : 'id="xs-miscellaneous-links"' }>
                            <li class="link">
                                <a href="miscellaneous/functions.html" data-type="entity-link">Functions</a>
                            </li>
                            <li class="link">
                                <a href="miscellaneous/variables.html" data-type="entity-link">Variables</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <a data-type="chapter-link" href="coverage.html"><span class="icon ion-ios-stats"></span>Documentation coverage</a>
                    </li>
                    <li class="divider"></li>
                    <li class="copyright">
                        Documentation generated using <a href="https://compodoc.app/" target="_blank">
                            <img data-src="images/compodoc-vectorise.png" class="img-responsive" data-type="compodoc-logo">
                        </a>
                    </li>
            </ul>
        </nav>
        `);
        this.innerHTML = tp.strings;
    }
});
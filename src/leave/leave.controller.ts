import {
  Body,
  Controller,
  Delete,
  Get,
  HttpException,
  HttpStatus,
  Param,
  Post,
  Put,
  Req,
  Res
} from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { UserLeave } from './entity/leave.entity';
import { User } from './entity/User.entity';
import { LeaveService } from './leave.service';
import { Response, Request } from 'express';
import { Reg } from './entity/login.entity';
import { customException } from '../custom exceptions/custom.exception';

@Controller('user')
export class LeaveController {
  constructor(
    private readonly leaveService: LeaveService,
    private jwtService: JwtService
  ) {}

  @Post()
  applyLeave(@Body() newdata: User) {
    return this.leaveService.applyLeave(newdata);
  }

  @Post('/login')
  async login(
    @Body() login: Reg,
    @Res({ passthrough: true }) response: Response
  ) {
    return this.leaveService.findOne(login, response);
  }

  @Post('/logout')
  async logout(@Res({ passthrough: true }) response: Response) {
    return this.leaveService.logout(response);
  }

  @Get('/userlogin')
  async user(@Req() request: Request) {
    //const data = await this.jwtService.verifyAsync(cookie);
    return this.leaveService.findUser(request);
  }

  @Get()
  getLeaveData(): Promise<User[]> {
    return this.leaveService.getLeaveData();
  }

  @Get('/:id')
  getLeavesData(@Param('id') id: number): Promise<UserLeave> {
    return this.leaveService
      .getLeavesById(id)
      .then((result) => {
        if (result) {
          return result;
        } else {
          throw new customException('user not found', HttpStatus.NOT_FOUND);
        }
      })

      .catch(() => {
        throw new customException('user not found', HttpStatus.NOT_FOUND);
      });
  }

  @Get('leavedata/:leaveDate')
  getByDate(@Param('leaveDate') leaveDate: string): Promise<User[]> {
    return this.leaveService
      .getLeaveByDate(leaveDate)
      .then((result) => {
        if (result) {
          return result;
        } else {
          throw new customException('user not found', HttpStatus.NOT_FOUND);
        }
      })

      .catch(() => {
        throw new customException('user not found', HttpStatus.NOT_FOUND);
      });
  }

  @Put('user/:id/:date')
  update(@Param('id') id: number, @Body() date: User) {
    return this.leaveService.updateUser(id, date);
  }

  @Delete('/:id')
  delete(@Param('id') id: number) {
    return this.leaveService.deleteLeave(id);
  }
}

import {
  BadRequestException,
  Injectable,
  Logger,
  Res,
  UnauthorizedException
} from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { InjectRepository } from '@nestjs/typeorm';
import { getConnection, Repository } from 'typeorm';
import { UserLeave } from './entity/leave.entity';
import { User } from './entity/User.entity';
import { Response, Request } from 'express';
import * as bcrypt from 'bcryptjs';
import { Reg } from './entity/login.entity';

@Injectable()
export class LeaveService {
  logger: Logger;
  constructor(
    @InjectRepository(UserLeave)
    private readonly leaveRepository: Repository<UserLeave>,
    @InjectRepository(User)
    private readonly dataRepository: Repository<User>,
    private jwtService: JwtService
  ) {
    this.logger = new Logger(LeaveService.name);
  }

  async getLeaveData(): Promise<User[]> {
    this.logger.log('all details added');
    return await this.dataRepository.find();
  }

  async applyLeave(newdata: User) {
    const leaveData = new UserLeave();
    const logindata = new Reg();
    const pass = await bcrypt.hash(newdata.password, 10);
    leaveData.totalLeaves = 15;
    leaveData.leavesTaken = newdata.noOfDays;
    leaveData.availableLeaves = leaveData.totalLeaves - leaveData.leavesTaken;
    const userData = new User();
    userData.password = logindata.password = pass;
    userData.name = logindata.name = newdata.name;
    userData.leaveDate = newdata.leaveDate;
    userData.noOfDays = newdata.noOfDays;
    userData.addLeave(leaveData);
    this.dataRepository.save(userData);
    this.logger.log('all leave details added');
    userData.reg = logindata;
    return `successfully applied leave for ${newdata.noOfDays}`;
  }

  async findOne(data: Reg, response: Response) {
    const user = await this.dataRepository.findOne({ name: data.name });
    console.log(user);
    if (!user) {
      throw new BadRequestException('invalid credentials');
    }
    if (!(await bcrypt.compare(data.password, user.password))) {
      throw new BadRequestException('invalid ');
    }
    const jwt = await this.jwtService.signAsync({ id: user.userId });
    response.cookie('jwt', jwt, { httpOnly: true });
    return {
      message: 'success'
    };
  }

  async logout(@Res({ passthrough: true }) response: Response) {
    response.clearCookie('jwt');
    return {
      message: 'logout'
    };
  }

  async findUser(request: Request) {
    try {
      const cookie = request.cookies['jwt'];
      const data = await this.jwtService.verifyAsync(cookie);
      if (!data) {
        throw new UnauthorizedException();
      }
      const user = await this.dataRepository.findOne({ userId: data.id });

      const { password, ...result } = user;

      return result;
    } catch (e) {
      throw new UnauthorizedException();
    }
  }

  async getLeavesById(id: number): Promise<UserLeave> {
    this.logger.log('Get leave data by using id');
    return await this.leaveRepository.findOne(id);
  }

  async getLeaveByDate(leavedate: string): Promise<User[]> {
    const leaves = await this.dataRepository.find({ leaveDate: leavedate });
    this.logger.log('Get leave by date');
    return leaves;
  }

  async deleteLeave(id: number) {
    // await getConnection()
    //   .createQueryBuilder()
    //   .delete()
    //   .from(UserLeave)
    //   .where('userIdUserId= :id', { id: id })
    //   .execute();
    // await getConnection()
    //   .createQueryBuilder()
    //   .delete()
    //   .from(User)
    //   .where('userId= :id', { id: id })
    //   .execute();
    await this.dataRepository.delete(id);
    this.logger.log('delete leave');
    return 'Deleted leave data succesfully';
  }

  updateUser(id: number, data: User) {
    const user = new User();
    user.name = data.name;
    user.leaveDate = data.leaveDate;
    user.noOfDays = data.noOfDays;

    this.dataRepository.update({ userId: id }, user);

    const leave = new UserLeave();
    const total = (leave.totalLeaves = 15);
    leave.leavesTaken = user.noOfDays;
    leave.availableLeaves = total - leave.leavesTaken;

    this.leaveRepository.update({ id: id }, leave);
    this.logger.log('update details');
    return 'successfully updated';
  }
}

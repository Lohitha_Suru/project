import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserLeave } from './entity/leave.entity';
import { User } from './entity/User.entity';
import { LeaveController } from './leave.controller';
import { LeaveService } from './leave.service';
import { Reg } from './entity/login.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([User, UserLeave, Reg]),
    JwtModule.register({
      secret: 'secret',
      signOptions: { expiresIn: '30m' }
    })
  ],
  controllers: [LeaveController],
  providers: [LeaveService]
})
export class LeaveModule {}

import { ApiProperty } from '@nestjs/swagger';
import { IsInt } from 'class-validator';
import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { User } from './User.entity';

@Entity()
export class UserLeave {
  @PrimaryGeneratedColumn()
  id: Number;

  @ApiProperty({
    type: Number,
    description: 'The address of the user',
    default: ''
  })
  @Column()
  @IsInt()
  availableLeaves: number;

  @ApiProperty({
    type: Number,
    description: 'The address of the user',
    default: ''
  })
  @Column()
  @IsInt()
  leavesTaken: number;

  @ApiProperty({
    type: Number,
    description: 'The address of the user',
    default: ''
  })
  @Column()
  @IsInt()
  totalLeaves: number;

  @ManyToOne(() => User, (user) => user.leave, { onDelete: 'CASCADE' })
  userId: User;
}

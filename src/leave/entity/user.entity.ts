import { ApiProperty } from '@nestjs/swagger';
import { IsInt, IsString } from 'class-validator';
import {
  Column,
  Entity,
  JoinColumn,
  LockNotSupportedOnGivenDriverError,
  OneToMany,
  OneToOne,
  PrimaryGeneratedColumn
} from 'typeorm';
import { UserLeave } from './leave.entity';
import { Reg } from './login.entity';

@Entity()
export class User {
  @PrimaryGeneratedColumn()
  userId: number;

  @ApiProperty()
  @Column()
  @IsString()
  name: string;

  @ApiProperty()
  @Column()
  @IsString()
  password: string;

  @ApiProperty()
  @Column()
  @IsString()
  leaveDate: string;

  @ApiProperty()
  @Column()
  @IsInt()
  noOfDays: number;

  @OneToOne(() => Reg, (reg) => reg.user, {
    cascade: true
  })
  reg: Reg;

  @OneToMany(() => UserLeave, (leave) => leave.userId, {
    cascade: true
  })
  @JoinColumn()
  leave: UserLeave[];

  addLeave(leave: UserLeave) {
    if (this.leave == null) {
      this.leave = new Array<UserLeave>();
    }
    this.leave.push(leave);
  }
}
